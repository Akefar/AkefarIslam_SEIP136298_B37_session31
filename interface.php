<?php

/**
 * An example of duck typing in PHP
 */

interface CanFly {
    public function fly();
}

interface CanSwim {
    public function swim();
}

class Bird {
    public $name;
    public function info() {
        echo "I am a {$this->name}\n";
        echo "I am an bird\n";
    }
}
class Penguine extends Bird implements CanSwim{
    public $name="penguine";
    public function swim(){
        echo "I can Swim <br>";
    }

}
class Dove extends Bird implements CanFly{
    public $name="DOVE";
    public function fly(){
        echo "I can Fly<br>";
    }
}
class Duck extends Bird implements CanFly,CanSwim{
    public $name="DUCK";
    public function swim(){
        echo "I can Swim <br>";
    }
    public function fly(){
        echo "I can Fly<br>";
    }
}


function describe($bird) {
    if ($bird instanceof Bird) {
        $bird->info();
        if ($bird instanceof CanFly) {
            $bird->fly();
        }
        if ($bird instanceof CanSwim) {
            $bird->swim();
        }
    } else {
        die("This is not a bird. I cannot describe it.");
    }
}

// describe these birds please
describe(new Penguine);
echo "---\n";

describe(new Dove);
echo "---\n";

describe(new Duck);

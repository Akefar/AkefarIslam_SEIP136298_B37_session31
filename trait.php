<?php
trait Hello{
    public function sayHello(){
        echo 'hello';
    }
}

trait World
{
    public function sayWorld()
    {
        echo 'World';
    }
}
class MyHelloWorld{
        use Hello,World;
        public function sayExclamationMark(){
        echo '!';
        }
    }

$o=new MyHelloWorld();
$o->sayHello();
$o->sayWorld();
$o->sayExclamationMark();
?>




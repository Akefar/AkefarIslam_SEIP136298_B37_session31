<?php
trait A {
    public function smallTalk() {
        echo 'a';
    }
    public function bigTalk() {
        echo 'A';
    }
}

trait B
{
    public function smallTalk()
    {
        echo 'b';
    }

    public function bigTalk()
    {
        echo 'B';
    }
}
trait C
{
    public function smallTalk()
    {
        echo 'c';
    }

    public function bigTalk()
    {
        echo 'C';
    }
}


class Talker {
    use A, B,C{
        B::smallTalk insteadof A;
        A::bigTalk insteadof B;
        C::bigTalk insteadof B;
    }
}


class Aliased_Talker {
    use A, B,C {
        B::smallTalk insteadof A;
        A::bigTalk insteadof B;
        B::bigTalk as talk;
        C::bigTalk insteadof B;
    }
}
$obj=new Aliased_Talker();
$obj->smallTalk();
$obj->bigTalk();
$obj->talk();
?>
